#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
    

    // Sprite information
    static const int SPRITE_HEIGHT = 32;
    static const int SPRITE_WIDTH = 24;

    // Bullet spawn
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    Game* game;

   
public:
    Player();
    ~Player();


    // health
    int health;
    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE};
    
    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);

    void changeSpeed(bool buttonPress);
    void setGame(Game* game);
    void fire();

    int getCurrentAnimationState();
    // damage function
    void takeDamage(int damage, bool instaKill);
    //Overloads
    void update(float timeDeltaInSeconds);

 // here i have moved int points to be public so it can be called with Game.cpp
    int points;

    // function for add score within te public to ensure the Player.cpp can use it
    void addScore(int points);
    int getScore();
};



#endif