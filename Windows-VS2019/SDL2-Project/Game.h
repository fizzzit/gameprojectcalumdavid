#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include <iostream>
#include <chrono>


#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class Vector2f;
class Timer
{
public:
    Timer()
    {
        m_StartTimepoint = std::chrono::high_resolution_clock::now();
    }
    ~Timer()
    {
        Stop();
    }
    void Stop()
    {
        auto endTimepoint = std::chrono::high_resolution_clock::now();

        auto start = std::chrono::time_point_cast<std::chrono::microseconds>(m_StartTimepoint).time_since_epoch().count();
        auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint).time_since_epoch().count();

        auto duration = end - start;
        double ms = duration * 0.001;

        std::cout << duration << "us (" << ms << "ms)\n";

    }

private:
    std::chrono::time_point< std::chrono::high_resolution_clock> m_StartTimepoint;
};


class Game 
{
private:
    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;

    // Background texture
    SDL_Texture*    backgroundTexture;
    
    //Declare Player
    Player*         player;

    //Declare NPC
    NPC*            npc;

    // Bullets
    vector<Bullet*> bullets;
    // new vector for NPC
    vector<NPC*> enemies;
    
    
    // Keyboard
    const Uint8     *keyStates;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();
    //timer
    float playerCollisionTimer;
    float collisionTime;


public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    void createBullet(Vector2f* position, Vector2f* velocity);
    void collisionDetection();
    void ShowScore();
    void update();
    
    // Window control in public 
    bool            quit;
    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 600;
};

#endif