#include "Game.h"
#include "TextureUtils.h"
#include "stdlib.h"
#include "Timer.h"

#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "Vector2f.h"
#include "AABB.h"

//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>


Game::Game() 
{
    Timer timer;
    gameWindow = nullptr;
    gameRenderer = nullptr;

    backgroundTexture = nullptr;
    player = nullptr;
    npc = nullptr;

    keyStates = nullptr;
    // the list has a length of 40, Max Enenmies
    enemies.resize(40);
    collisionTime = 1;

    quit = false;
}


void Game::init()
{
    Timer timer;
     gameWindow = SDL_CreateWindow("Alien Swams Survival",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }
    
    // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/Alien background.png", gameRenderer);

    if(backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);
     // setup for multpiles NPCs
    for (int i = 0; i < enemies.size(); i++)
    {
        enemies[i] = new NPC();
        enemies[i]->init(gameRenderer);
        enemies[i]->setGame(this);
        enemies[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
    }
    
    //npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;  

    for (int i = 0; i < enemies.size(); i++)
    {
        delete enemies[i];
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end();)
    {   
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
    } 
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Game::draw()
{
     // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

    player->draw(gameRenderer);
    
    for (int i = 0; i < enemies.size(); i++)
        enemies[i]->draw(gameRenderer);

    for(int i = 0; i < bullets.size(); i++)
        bullets[i]->draw(gameRenderer);

    // 2.5 Draw HUD
   // printf("Player Score: %d\n", player->getScore());

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    
}

void Game::update(float timeDelta)
{
    player->update(timeDelta);

   // if(npc->isDead())
   //     npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    for (int i = 0; i < enemies.size(); i++)
        enemies[i]->update(timeDelta);

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        if((*it)->hasExpired())
        {
            delete *it;
            *it = nullptr;
            it = bullets.erase(it);
        }
        else
        {
            ++it;
        }
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end(); ++it)
    {
        (*it)->update(timeDelta);
    }

    collisionDetection();
    // if player health is equal to 0, end game as the player is dead
    if (player->health <= 0)
    {
        printf("\nPlayer has Died");
        quit = true;
        
    }

    playerCollisionTimer += timeDelta;
    // here i am using an if function to have the game exit if the player reaches a score of 2000, 
    //as this score would mean all enemies are dead
    if (player->points >= 2000)
    {  
        quit = true;
        printf_s("Well done! You have Defeated all Enemies and Completed the Game \n");
    }

}
// void function to proccess all the inputs and functions that come with the corrsponding inputs.
// used for movement of player
void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
            case SDLK_LSHIFT:
                player->changeSpeed(true);
                break;
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                case SDLK_LSHIFT:
                    player->changeSpeed(false);
                    break;
                }

            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break; 
                
        }
    }

    // Process Inputs - for player 
    player->processInput(keyStates);
}

Player* Game::getPlayer() 
{
    return player;
}


void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise prevTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}
// void game function to create the bullet, and render
void Game::createBullet(Vector2f* position, Vector2f* velocity)
{
    Bullet* bullet = new Bullet();

    bullet->init(gameRenderer, position, velocity);

    bullets.push_back(bullet);
}

void Game::ShowScore()
{
    // this is where the end goal will be set
    printf("Player Score: %d\n", player->getScore());


}


void Game::collisionDetection() // for loop to check for collision between all enemies 
{
    for (int i = 0; i < enemies.size(); i++)
    {
        for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
        {
            if (enemies[i]->getAABB() != nullptr)
            {

                if ((*it)->getAABB()->intersects(enemies[i]->getAABB()) && enemies[i]->state != enemies[i]->DEAD)
                {


                    // damage npc
                    enemies[i]->takeDamage((*it)->getDamage());

                    // destroy arrow (or it'll keep hitting!)
                    delete* it;
                    *it = nullptr;
                    it = bullets.erase(it);
                }
                else
                {
                    ++it;
                }
            }
        }
        // colision detection for when the enemy NPCs touch Player Sprite.
        if (enemies[i]->getAABB()->intersects(player->getAABB()) && playerCollisionTimer >= collisionTime)
        { // true = instaKill false just the health take off
            player->takeDamage(10, false);
            playerCollisionTimer = 0;
        }
    }
}
//void Game::end()
//{
   // if (player->getScore() >= 20)
  //  {
  //      return;
  // }
//}



//void Game::update()
//{
  //  if (
    //    NPC::NPCState::DEAD
//}


/* Just comments for git repo
* fork depo, then clone "cd XXXXXXXXXXXXXX"
* then to add comments 
* git add . 
* git commit -m " Message XX" 
* git push */
