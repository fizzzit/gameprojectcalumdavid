#include "Bullet.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"

#include <stdexcept>
#include <string>
#include <cmath> //for acos
#include <iostream>

using std::string;

/**
 * Bullet
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Bullet::Bullet() : Sprite()
{
    state = TRAVEL;
    speed = 100.0f;

    orientation = 0.0f;
    damage = 10;
    lifetime = 5.0f;
    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    //Need to change this based on the image
    //being used.
    angleOffset = -90.0f;

}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Bullet::init(SDL_Renderer *renderer, Vector2f* position, Vector2f* direction)
{
    // set orientation
    this->orientation = calculateOrientation(direction);

    //std::cout << orientation << std::endl;

    //path string
    string path("assets/images/bullet-row.png");

    // Call sprite constructor
    Sprite::init(renderer, path, 1, position);

    // set velocity to forward direction of travel
    velocity->setX(direction->getX());
    velocity->setY(direction->getY());
    velocity->normalise();
    velocity->scale(speed);

    // Setup the animation structure
    animations[TRAVEL]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

float Bullet::calculateOrientation(Vector2f* direction)
{
    float angle = atan2f(direction->getY(), direction->getX()); //get angle (0,2pi)
    angle*=(180.0f/3.142f); //convert to degrees
    return angle+angleOffset;
}

/**
 * ~Bullet
 * 
 * Destroys the Bullet and any associated 
 * objects 
 * 
 */
Bullet::~Bullet()
{

}

void Bullet::update(float dt)
{
    lifetime -= dt;
    Sprite::update(dt);
}

void Bullet::draw(SDL_Renderer *renderer)
{
    // Get current animation based on the state.
    Animation *current = this->animations[getCurrentAnimationState()];

    //SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
    // Use extended version of SDL_RenderCopy which can rotate and flip!
    SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);
}
// int get current Animation State function
int Bullet::getCurrentAnimationState()
{
    return state;
}
// int get damage function
int Bullet::getDamage()
{
    return damage;
}

bool Bullet::hasExpired()
{
    if(lifetime > 0.0f)
        return false;
    else
        return true;
}
