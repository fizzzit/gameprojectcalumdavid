#ifndef NPC_H_
#define NPC_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;


class NPC : public Sprite
{
private:
    
   
        
    // Sprite information
    static const int SPRITE_HEIGHT = 48;
    static const int SPRITE_WIDTH = 56;

    // Need game
    Game* game;

    // weapon range
    float maxRange;
    float timeToTarget;
    
    // npc health
    int health;

    // point value
    // set this at creation?
    int points;

public:
    NPC();
    ~NPC();

    // Player Animation states
    enum NPCState{LEFT=0, RIGHT, UP, DOWN, IDLE, DEAD};
    
    void init(SDL_Renderer *renderer);
    void update(float timeDeltaInSeconds);

    // Update 'things' ai related
    
    void ai();
    void setGame(Game* game);

    int getCurrentAnimationState();

    // damage / death / respawn
    void takeDamage(int damage);
    bool isDead();
    void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);
    
    int getPoints();
    void setPoints(int pointValue);

    // Animation state, able to check when colliding whether the enemy is dead so that the bullet can pass through
    int state;
    
};

#endif